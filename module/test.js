function header(arr, rule){
    arr.push({
        title: pdfh(getResCode(), rule.标题),
        url: rule.链接 ? pd(getResCode(), rule.链接) : MY_URL,
        pic: pd(getResCode(), rule.图片),
        desc: pdfh(getResCode(), rule.描述),
        col_type: "movie_1_vertical_pic"
    })
}
header